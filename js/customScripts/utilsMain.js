function openLogin()
{
    $("#modal").show(); 
    $("#modal").modal({show:true}); 
    $(".modal-backdrop").show();
}

function closeLogin()
{
    $('#modal').hide();
    $(".modal-backdrop").hide();
}
             
function getSingleBookRequest(bookId)
{
    return $.ajax({
        url: "http://localhost:8092/books/"+bookId,
        type: 'GET',
        headers:{
            'Content-Type':'Application/json'
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("tokenAuth"))
        },
        dataType: "json",
        /*success: function (data) {
         console.log('success' + JSON.stringify(data));
         $('response').append(JSON.stringify(data))
                $("#result").append("<tr>" 
                + "<td>" + data.name + "</td>" 
                + "<td>" + data.author + "</td>" 
                + "<td>" + data.brief + "</td>" 
                + "</tr>"); 
        },
        error: function (data) {
         console.log('error' + JSON.stringify(data));

        }*/
    });
    
};
 
function getSingleBookInfo(bookId)
{

    return getSingleBookRequest(bookId).done(
        function(data, statusText, xhr){
            console.log("test2", JSON.stringify(data));
                
            /*$('response').append(JSON.stringify(data))
            $("#result")*/
            jsonString = "<tr>" 
            + "<td>" + data.name + "</td>" 
            + "<td>" + data.author + "</td>" 
            + "<td>" + data.brief + "</td>" 
            + "</tr>";
            return jsonString;
        }
    );
    
}           